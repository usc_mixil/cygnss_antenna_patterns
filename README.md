# README #

These files are binary files containing the gain in dB for each CYGNSS antenna.  
There is a value for each azimuth from 0 to 359.9 degrees, spaced at 0.1 degree increments.  
There is a value for each elevation from -90 to 90 degrees, spaced at 0.1 degree increments.  
The first entry in the file is for az = 0, elev = -90. The second will be for az = 0.1, elev = -90 etc.  
